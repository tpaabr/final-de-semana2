class Student < ApplicationRecord
    
    has_many :subcriptions
    has_many :classrooms, through: :subcriptions
    belongs_to :user
    #validates_length_of :registration, :minimum => 9 
end
