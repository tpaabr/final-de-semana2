class Subject < ApplicationRecord
    has_many :licenses
    has_many :teachers, through: :licenses
    has_one :classroom

    #Auto Relacionamnto
    has_many :requirements
    has_many :prerequisites, class_name: 'Subject'
end
