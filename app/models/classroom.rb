class Classroom < ApplicationRecord

    validate :teacher_has_license
    belongs_to :subject
    belongs_to :teacher, optional: true

    has_many :subcriptions
    has_many :students, through: :subcriptions

    enum status: [
        "construction_in_progress", "active"
    ]

    def teacher_has_license
        if teacher_id != nil
            objeto = License.where(teacher_id: teacher_id, subject_id: subject_id)
            if objeto.length() == 0
                errors.add(:teacher_id, "This teacher dont have a license for this subject")
            end
        end
    end

end
