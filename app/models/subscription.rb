class Subscription < ApplicationRecord

  validate :is_classroom_in_construction, :student_has_prerequisites, :can_insert_grades
  belongs_to :student
  belongs_to :classroom

  enum status: [
    "in_progress", "done"
  ]

  def is_classroom_in_construction
    current_classroom = Classroom.find(classroom_id)
    if current_classroom.status == "active" and status == "in_progress"
      errors.add(:classroom_id, "Cant subscribe to active class")
    end
  end

  def student_has_prerequisites

  end

  def can_insert_grades
    if (p1 != nil or p2 != nil) and status == "in_progress"
      errors.add(:status, "Cant change grade of inactive subscription")
    end
  end
end
