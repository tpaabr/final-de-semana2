class Requirement < ApplicationRecord
    validate :cant_be_prerequisite_to_itself, :cant_have_circular_relation

    def cant_be_prerequisite_to_itself
      if subject_id == prerequisite_id
        errors.add(:prerequisite_id, "can't be prerequisite to itself")
      end
    end

    def cant_have_circular_relation
      #Ainda falta resolver caso exista mais de uma materia entre a relacao circular

      tamanhoTabela = Requirement.count
      
      objectList = Requirement.where(subject_id: prerequisite_id).take(tamanhoTabela)

      if objectList.length == 0
        return true
      end
      objectList.each do |i|
        if i.prerequisite_id == subject_id
          errors.add(:subject_id, "can't have circular relation")
        end
      end
      
    end


    belongs_to :subject
    belongs_to :prerequisite, class_name: "Subject"
end
