class User < ApplicationRecord
  has_one :teacher
  has_one :student
  validates_format_of :email, :with => /[0-9a-z_]+@id.uff.br/
  validates_length_of :password, minimum: 6
  devise :database_authenticatable,
        :jwt_authenticatable,
        jwt_revocation_strategy: JwtBlacklist


  enum kind: [
    "student", "secretary", "teacher", "admin"
  ]

  after_create :createObject
  def createObject
    
    if kind == "student"
      Student.create!(user_id: id)
    elsif kind == "teacher"
      Teacher.create!(user_id: id)
    end

  end
end
