class Teacher < ApplicationRecord
    belongs_to :user
    has_one :classroom
    
    has_many :licenses
    has_many :subjects, through: :licenses
    #validates_length_of :registration, :minimum => 9
end
