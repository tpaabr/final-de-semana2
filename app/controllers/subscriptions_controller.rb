class SubscriptionsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_subscription, only: [:show, :update, :destroy]

  # GET /subscriptions
  def index
    @subscriptions = Subscription.all

    render json: @subscriptions
  end

  # GET /subscriptions/1
  def show
    render json: @subscription
  end

  # POST /subscriptions
  def create
    @subscription = Subscription.new(subscription_params)

    if @subscription.save
      render json: @subscription, status: :created, location: @subscription
    else
      render json: @subscription.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /subscriptions/1
  def update
    if @subscription.update(subscription_params)
      render json: @subscription
    else
      render json: @subscription.errors, status: :unprocessable_entity
    end
  end

  #PUT /subcriptions/grade/:id
  #ATUALIZAR FINALGRADE ao ATUALIZAR p1 e p2
  def grade
    if @subscription.update(subscription_params)
      
      current_student = Student.find(@subscription.student_id)

      @subscription.update_attribute(:finalgrade, (@subscription.p1 + @subscription.p2)/2)
      #ATUALIZAR O CR
      if current_student.cr == nil
        current_student.update_attribute(:cr, @subscription.finalgrade)
      else
        current_student.update_attribute(:cr, (current_student.cr + @subscription.finalgrade)/2)
      end
      

      render json: @subscription
    else
      render json: @subscription.errors, status: :unprocessable_entity
    end
  end

  # DELETE /subscriptions/1
  def destroy
    @subscription.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subscription
      @subscription = Subscription.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def subscription_params
      params.require(:subscription).permit(:student_id, :classroom_id, :p1, :p2, :status, :finalgrade)
    end
end
