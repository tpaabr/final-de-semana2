class ClassroomsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_classroom, only: [:show, :update, :destroy]
  

  # GET /classrooms
  def index
    @classrooms = Classroom.all

    render json: @classrooms
  end

  #GET /classrooms/available
  def show_available
    @available = Classroom.where(status: "construction_in_progress")
    render json: @available
  end

  # GET /classrooms/1
  def show
    render json: @classroom
  end

  # POST /classrooms
  def create
    @classroom = Classroom.new(classroom_params)

    if @classroom.save
      render json: @classroom, status: :created, location: @classroom
    else
      render json: @classroom.errors, status: :unprocessable_entity
    end

    

  end

  # PATCH/PUT /classrooms/1
  def update
    if @classroom.update(classroom_params)
      render json: @classroom
    else
      render json: @classroom.errors, status: :unprocessable_entity
    end

    puts 'oi'
    
    #Dar update na inscricao para valida
    if @classroom.status == "active"
      
      @object_list = Subscription.where(classroom_id: @classroom.id)
  
      @object_list.each do |i|
        i.update_attribute(:status, "done")
      end

    end
  end

  # DELETE /classrooms/1
  def destroy
    @classroom.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_classroom
      @classroom = Classroom.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def classroom_params
      params.require(:classroom).permit(:code, :schedule, :status, :monday, :tuesday, :wednesday, :thursday, :friday, :subject_id, :teacher_id)
    end
end
