Rails.application.routes.draw do
  resources :requirements
  resources :licenses
  resources :subscriptions
  resources :subjects
  resources :teachers
  resources :students

  get '/classrooms/available', to: 'classrooms#show_available'
  put '/subscriptions/grade/:id', to: 'subscriptions#grade'
  resources :classrooms

  resources :users
  devise_for :users,
             path: '',
             path_names: {
               sign_in: 'login',
               sign_out: 'logout',
             },
             controllers: {
               sessions: 'sessions',
             }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
