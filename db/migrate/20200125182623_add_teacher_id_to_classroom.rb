class AddTeacherIdToClassroom < ActiveRecord::Migration[5.2]
  def change
    add_reference :classrooms, :teacher, foreign_key: true
  end
end
