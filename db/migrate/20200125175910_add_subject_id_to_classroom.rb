class AddSubjectIdToClassroom < ActiveRecord::Migration[5.2]
  def change
    add_reference :classrooms, :subject, foreign_key: true
  end
end
