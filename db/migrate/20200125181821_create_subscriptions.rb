class CreateSubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :subscriptions do |t|
      t.references :student, foreign_key: true
      t.references :classroom, foreign_key: true
      t.float :p1
      t.float :p2
      t.integer :status
      t.float :finalgrade

      t.timestamps
    end
  end
end
