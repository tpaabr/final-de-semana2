class RemoveStatusFromClassroom < ActiveRecord::Migration[5.2]
  def change
    remove_column :classrooms, :status, :integer
  end
end
