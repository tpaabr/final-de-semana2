class AddStatusToClassroom < ActiveRecord::Migration[5.2]
  def change
    add_column :classrooms, :status, :integer, default: 0
  end
end
