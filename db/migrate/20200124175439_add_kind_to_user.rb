class AddKindToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :kind, :integer, default: 'aluno'
    add_column :users, :name, :string
  end
end
