# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(email: 'admin@id.uff.br', name: 'ADMIN', password: '123456', kind:"admin")
User.create(email: 'secretary@id.uff.br', name: 'secretary', password: '123456', kind:'secretary')
User.create(email: 'aluuuuno@id.uff.br', name: 'alunoTeste', password: '12345678', kind:'student')
User.create(email: 'pruf@id.uff.br', name: 'profTeste', password: '12345678', kind:'teacher')
Subject.create(department: 'GMA', name: "calculo 1")
Subject.create(department: 'GMA', name: "calculo 2")
Subject.create(department: 'GMA', name: "calculo 3")
Subject.create(department: 'GMA', name: "Algebra Linear")
Subject.create(department: 'GMA', name: "Geometria Analitica")
Classroom.create(code: "A1", schedule: "13:00", monday: true, tuesday: false, wednesday: true, thursday: false, friday: false, status: "construction_in_progress")
